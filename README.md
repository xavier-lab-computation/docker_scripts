# Docker Scripts
Repository of commonly used code for easy docker access.

**This script is no longer used in bulk_rna_seq pipeline. Find its own version in its independent repository.**
<hr>

# Adding code to docker
You can add this repository to a docker container by including the following in your `Dockerfile`:
```
# add standard scripts to docker
RUN git clone https://gitlab.com/xavier-lab-computation/docker_scripts.git && \
    mv docker_scripts/* <directory/where/you/want/code/>
```

<hr>

# Rebuilding dockers
When one of the files in this directory is updated, we recommend that you rebuild every docker image that uses the file in question. Here is a list of the [pipelines](https://gitlab.com/xavier-lab-computation/pipelines) that rely on each file in the repository:

    - run_edgeR.R
        - edgeR_differential_expression
        - stars_crispr_screen

See [here](https://xavier-lab.readthedocs.io/en/master/03_pipelines/usage/building_editing_a_pipeline.html#editing-a-docker-container) for tips on editing a docker image.

> **_NOTE:_**
Christian is not sure that simply attempting to rebuild the docker image will work, as it might not realize that the git repository has changed. One hack to get around this is to change a small thing right before the line in the `Dockerfile` where the docker scripts are cloned. For example, adding or removing a comment should be sufficient.
